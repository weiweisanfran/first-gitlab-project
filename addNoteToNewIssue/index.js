
const https = require('https');




exports.handler =  (event, context, callback) => {

    
    
    const message = 'Thank%20you%20for%20creating%20the%20issue.';
    
 
    var reqbody = event.body;

    var created_at = reqbody && reqbody.object_attributes ? reqbody.object_attributes.created_at : null;
    var updated_at = reqbody && reqbody.object_attributes ? reqbody.object_attributes.updated_at : null;
    var needToInsertWelcomeComment = false;
    
    if (created_at!=null && updated_at!=null && (created_at == updated_at))
        needToInsertWelcomeComment = true;
    

    //only insert comment if issue is new    
    if (needToInsertWelcomeComment) {

        var username = reqbody && reqbody.user ? reqbody.user.name : null;
        var project_id = reqbody && reqbody.project ? reqbody.project.id : null;
        var issue_id = reqbody && reqbody.object_attributes ? reqbody.object_attributes.iid : null;
        var path_str = '/api/v4/projects/'+ project_id + '/issues/' + issue_id + '/notes?body=' + message;
            
        // console.log('\r\nweiwei log: user name = ' + username);
        // console.log('\r\nweiwei log: project id = ' + project_id);
        // console.log('\r\nweiwei log: issue_id = ' + issue_id);
    
        var post_options = {
                        host: 'gitlab.com',
                        path: path_str,
                        // path: '/api/v4/projects/12926890/issues/2/notes?body=Thankyouforcreatinganissue',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'PRIVATE-TOKEN': 'fMMnyo5tMHZTGtxEynd9'
                        }
        };
        var post_req = https.request(post_options, function() {});
        post_req.end();
    }
      

    callback(null, 'GitLab Comment Bot: ' + path_str + "\r" + reqbody); 

};
